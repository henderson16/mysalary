package com.henderson.mysalary;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.henderson.mysalary.utils.Constant;


public class BaseActivity extends AppCompatActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	protected void showProgressBar(ProgressBar bar){
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		bar.setVisibility(View.VISIBLE);

	}

	protected void hideProgressBar(ProgressBar bar){
		bar.setVisibility(View.GONE);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
	}

/*	private long interval = 1000;
	private long previous = 0;
	private long current = 0; */
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		/*
		current = System.currentTimeMillis();
		if(current - previous < interval) {
			super.onBackPressed();
		}
		else {
			previous = current;
			toast(R.string.back_button);
		}
		*/
	}


	//Toolbar Item Event
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	//Example : log("getLocalIpAddress", "ip : " + ip);
	protected void log(String methodName, String msg){
		if(BuildConfig.DEBUG) Log.d("!!" + getClass().getSimpleName(), methodName + " " + msg);
	}
}
