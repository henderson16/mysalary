package com.henderson.mysalary.pay;

import com.henderson.mysalary.model.Month;
import com.henderson.mysalary.model.SalaryInfo;

import java.util.ArrayList;

public interface PayListener {
	void setCalendar(Month month);
	void setPaydayInfo(ArrayList<SalaryInfo> payday);
}
