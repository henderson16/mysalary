package com.henderson.mysalary.pay.detail;

public interface DetailSalaryListener {
	void finishInitData();
	void clickWeeklyBonusDetail();
}
