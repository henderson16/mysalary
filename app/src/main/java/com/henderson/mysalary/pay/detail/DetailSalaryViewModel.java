package com.henderson.mysalary.pay.detail;

import android.app.Activity;
import android.app.Application;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.MutableLiveData;

import com.henderson.mysalary.BaseViewModel;
import com.henderson.mysalary.R;
import com.henderson.mysalary.model.DailyPay;
import com.henderson.mysalary.utils.BaseUtil;
import com.henderson.mysalary.utils.Constant;
import com.henderson.mysalary.utils.StringUtil;
import com.henderson.mysalary.utils.db.AppDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import static com.henderson.mysalary.utils.StringUtil.addComma;

public class DetailSalaryViewModel extends BaseViewModel {

	public DetailSalaryListener detailSalaryListener;

	public DetailSalaryViewModel(@NonNull Application application) {
		super(application);
	}

	public Constant.SalaryType salaryType;
	public String jobName;
	public int jobPayday;
	public int thisYear;
	public int thisMonth;
	public int thisMonthFirstDay;
	public int prevYear;
	public int prevMonth;
	public int prevMonthFirstDay;

	public List<DailyPay> payList = new ArrayList<>();
	public String salaryPeriod;
	public MutableLiveData<Integer> totalSalary = new MutableLiveData<>();
	public MutableLiveData<Integer> tax = new MutableLiveData<>();
	public MutableLiveData<Integer> weeklyBonus = new MutableLiveData<>(0);
	public MutableLiveData<String> detailWeeklyBonus = new MutableLiveData<>();
	public MutableLiveData<Integer> realSalary = new MutableLiveData<>();
	public MutableLiveData<Boolean> insuranceSwitch = new MutableLiveData<>(true);
	public MutableLiveData<Boolean> weeklyBonusSwitch = new MutableLiveData<>(true);
//	public double totalSalary;
//	public double tax;
//	public double realSalary;

	public LinkedHashMap<String, Integer> weeklyBonusMap = new LinkedHashMap<>();

	@Override
	protected void onCreate(Activity activity) {
		super.onCreate(activity);

		jobName = activity.getIntent().getStringExtra(Constant.JOBNAME);
		thisYear = activity.getIntent().getIntExtra(Constant.THISYEAR, -1);
		thisMonth = activity.getIntent().getIntExtra(Constant.THISMONTH, -1);
		thisMonthFirstDay = activity.getIntent().getIntExtra(Constant.THISMONTH_FIRST_DAY, -1);
		prevYear = activity.getIntent().getIntExtra(Constant.PREVYEAR, -1);
		prevMonth = activity.getIntent().getIntExtra(Constant.PREVMONTH, -1);
		prevMonthFirstDay = activity.getIntent().getIntExtra(Constant.PREVMONTH_FIRST_DAY, -1);
		jobPayday = AppDatabase.getAppDatabase(getApplication()).getJobPayday(jobName);

		if (jobPayday - 1 == 0) {
			salaryPeriod = prevMonth + "월 " + jobPayday + "일 ~ " + prevMonth + "월 " + "말일";
		} else {
			salaryPeriod = prevMonth + "월" + jobPayday + "일 ~ " + thisMonth + "월" + (jobPayday - 1) + "일";
		}

		initData();
	}

	protected void initData() {
		payList = new ArrayList<>();
		List<DailyPay> prevDailyPay;
		prevDailyPay = AppDatabase.getAppDatabase(getApplication()).getDailyPayOrderBy(salaryType.getValue(), jobName, prevYear, prevMonth);
		List<DailyPay> thisDailyPay;
		thisDailyPay = AppDatabase.getAppDatabase(getApplication()).getDailyPayOrderBy(salaryType.getValue(), jobName, thisYear, thisMonth);
		this.payList.addAll(prevDailyPay);
		this.payList.addAll(thisDailyPay);

		if (salaryType == Constant.SalaryType.HOURLY_PAY) setWeeklyBonus();
		setWorkingList();
		calculateSalaryInfo();
		detailSalaryListener.finishInitData();
	}

	private void setWorkingList() {
		for (int i = 0; i < payList.size(); i++) {
			if (payList.get(i).month == prevMonth) {
				if (payList.get(i).date < jobPayday) {
					payList.remove(i);
					i--;
				}
			} else {
				if (payList.get(i).date >= jobPayday) {
					payList.remove(i);
					i--;
				}
			}
		}
	}

	private LinkedHashMap<String, Integer> getPrevOfPrevWeeklyBonus(int baseDate) {
		LinkedHashMap<String, Integer> popWeeklyBonus = new LinkedHashMap<>();

		int _prevYear = prevYear;
		int _prevMonth = prevMonth;
		int _sum = 0;
		if (_prevMonth - 1 <= 0) {
			_prevMonth = 12;
			int _popLastDate = BaseUtil.lastDateOfMonth(_prevYear - 1, _prevMonth);
			int _popKey = _popLastDate + baseDate;
			int _date = _popKey;

			for (int i = 0; i < 7; i++) {
				if(_prevMonth>12) _prevMonth -=12;
				DailyPay _dailyPay = AppDatabase.getAppDatabase(activity).getDailyPay(0, jobName, _prevYear-1, _prevMonth, _date);
				if(_dailyPay != null) _sum += _dailyPay.workingTime;
				_date++;
				if (_date > _popLastDate)  {
					_date = 1;
					_prevYear++;
					_prevMonth++;
				}
			}
			popWeeklyBonus.put(12 + " " + _popKey, _sum);
			return popWeeklyBonus;
		} else {
			int _popLastDate = BaseUtil.lastDateOfMonth(_prevYear, _prevMonth - 1);
			int _popKey = _popLastDate + baseDate;
			int _date = _popKey;

			for (int i = 0; i < 7; i++) {
				DailyPay _dailyPay = AppDatabase.getAppDatabase(activity).getDailyPay(0, jobName, _prevYear, _prevMonth-1, _date);
				if(_dailyPay != null) _sum += _dailyPay.workingTime;
				_date++;
				if (_date > _popLastDate)  {
					_date = 1;
					_prevMonth++;
				}
			}
			popWeeklyBonus.put(prevMonth - 1 + " " + _popKey, _sum);
			return popWeeklyBonus;
		}
	}

	/**
	 * 주휴 수당 계산 로직
	 * 월~일까지가 기본
	 * 기준 주부터 한 주씩 근무 시간 계산
	 * List를 순회하며 주휴수당 계산
	 */
	protected void setWeeklyBonus() {
		if (weeklyBonusSwitch.getValue()) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, prevYear);
			calendar.set(Calendar.MONTH, prevMonth - 1);
			calendar.set(Calendar.DATE, jobPayday);
			//주휴 수당 기준일 : 월 (월~일)
			int day = calendar.get(Calendar.DAY_OF_WEEK);
			int baseDate;
			switch (day) {
				case Calendar.SUNDAY:
					baseDate = jobPayday - 6;
					break;
				case Calendar.MONDAY:
					baseDate = jobPayday;
					break;
				case Calendar.TUESDAY:
					baseDate = jobPayday - 1;
					break;
				case Calendar.WEDNESDAY:
					baseDate = jobPayday - 2;
					break;
				case Calendar.THURSDAY:
					baseDate = jobPayday - 3;
					break;
				case Calendar.FRIDAY:
					baseDate = jobPayday - 4;
					break;
				case Calendar.SATURDAY:
					baseDate = jobPayday - 5;
					break;
				default:
					baseDate = jobPayday;
					break;
			}

			weeklyBonusMap.clear();
			//전 전 달의 주휴 수당 계산이 필요할 때
			if (baseDate <= 0) {
				weeklyBonusMap = getPrevOfPrevWeeklyBonus(baseDate);
				baseDate += 7;
			}

			//주휴수당 키 설정
			int lastDateOfMonth = BaseUtil.lastDateOfMonth(prevYear, prevMonth);
			int _keyMonth = prevMonth;
			while (true) {
				if(_keyMonth + 1 > 12) {
					if ((baseDate + 6) > lastDateOfMonth && _keyMonth+1-12 == thisMonth && baseDate + 6 - lastDateOfMonth >= jobPayday ) {
						break;
					}
				}
				else if ((baseDate + 6) > lastDateOfMonth && _keyMonth+1 == thisMonth && baseDate + 6 - lastDateOfMonth >= jobPayday ) {
					break;
				}
				if (_keyMonth == thisMonth && (baseDate + 6) >= jobPayday) {
					break;
				}

				weeklyBonusMap.put(_keyMonth + " " + baseDate, 0);
				baseDate += 7;

				if (baseDate > lastDateOfMonth) {
					_keyMonth++;
					if(_keyMonth>12) {_keyMonth -= 12;}
					baseDate = baseDate - lastDateOfMonth;
				}
			}

			//주휴 근무 시간 계산
			baseDate = 0;
			for (int i = 0; i < payList.size(); i++) {
				DailyPay dailyPay = payList.get(i);
				int date = BaseUtil.dayOfWeek(dailyPay.year, dailyPay.month, dailyPay.date);

				switch (date) {
					case Calendar.SUNDAY:
						baseDate = dailyPay.date - 6;
						break;
					case Calendar.MONDAY:
						baseDate = dailyPay.date;
						break;
					case Calendar.TUESDAY:
						baseDate = dailyPay.date - 1;
						break;
					case Calendar.WEDNESDAY:
						baseDate = dailyPay.date - 2;
						break;
					case Calendar.THURSDAY:
						baseDate = dailyPay.date - 3;
						break;
					case Calendar.FRIDAY:
						baseDate = dailyPay.date - 4;
						break;
					case Calendar.SATURDAY:
						baseDate = dailyPay.date - 5;
						break;
					default:
						break;
				}

				String key = dailyPay.month + " " + baseDate;

				if (weeklyBonusMap.containsKey(key)) {
					int value = weeklyBonusMap.get(key);
					weeklyBonusMap.put(key, value + dailyPay.workingTime);
				}
			}

			//주휴 수당 계산 & 세부 주당 근무 시간 설정
			double _totalWeeklyBonus = 0;
			String _detailWeeklyBonus = "";
			int _jobPayment = AppDatabase.getAppDatabase(getApplication()).getJobPayment(jobName);
			for (String key : weeklyBonusMap.keySet()) {
				int _workingHour = weeklyBonusMap.get(key) / 60;
				double _weeklyBonus = 0;
				if (_workingHour >= 40) {
					_weeklyBonus = 8 * _jobPayment;
				} else if (_workingHour >= 15) {
					_weeklyBonus = (_workingHour / 40.0) * 8 * _jobPayment;
				}
				_totalWeeklyBonus += _weeklyBonus;

				String str[] = key.split(" ");
				int month = Integer.parseInt(str[0]);
				int date = Integer.parseInt(str[1]);

				String temp = String.format("%2s월%2s일~ 주간 근무 시간:%3s = 수당: %s원\n", month, date, _workingHour, addComma((int) _weeklyBonus));
				_detailWeeklyBonus += temp;
			}
			weeklyBonus.setValue((int) _totalWeeklyBonus);
			detailWeeklyBonus.setValue(_detailWeeklyBonus);
		} else {
			weeklyBonus.setValue(0);
			detailWeeklyBonus.setValue(null);
		}
	}

	protected void calculateSalaryInfo() {
		int _totalSalary = 0;
		int _weeklyBonus = weeklyBonus.getValue();
		int _tax = 0;
		int _realSalary = 0;
		for (int i = 0; i < payList.size(); i++) {
			switch (this.salaryType) {
				case HOURLY_PAY:
					_totalSalary += payList.get(i).hourlyPayment * payList.get(i).workingTime / 60;
					break;
				case DAILY_PAY:
					_totalSalary += payList.get(i).dailyPayment;
					break;
			}
		}

		_totalSalary += _weeklyBonus;

		if (insuranceSwitch.getValue()) {
			_tax = (int) (_totalSalary * 0.033);
		} else {
			_tax = 0;
		}

		_realSalary = _totalSalary - _tax;

		totalSalary.setValue(_totalSalary);
		tax.setValue(0 - _tax);
		realSalary.setValue(_realSalary);
	}

	protected void updateDailyPay() {
		initData();
		BaseUtil.toast(activity, R.string.toast_success_update);
	}

	protected void deleteDailyPay(int id) {
		boolean result = AppDatabase.getAppDatabase(getApplication()).deleteDailyPay(id);
		log("deleteDailyPay", "result " + result);

		initData();
		BaseUtil.toast(activity, R.string.toast_success_delete);
	}

	public void setWeeklyVisibility(View view) {
		detailSalaryListener.clickWeeklyBonusDetail();
	}

	public void showWeeklyInfo(View view){
		BaseUtil.showDialog(activity, R.string.toast_info_weeklybonus, R.string.ok).show();
	}

	@BindingAdapter("itos")
	public static void itos(TextView view, int value) {
		if (value < 0) {
			view.setText("- " + addComma(value * -1) + view.getContext().getResources().getString(R.string.won));
		} else {
			view.setText(addComma(value) + view.getContext().getResources().getString(R.string.won));
		}
	}
}
