package com.henderson.mysalary.pay;

import android.app.Activity;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.henderson.mysalary.BaseViewModel;
import com.henderson.mysalary.R;
import com.henderson.mysalary.utils.StringUtil;

public class ExpectedPayViewModel extends BaseViewModel {

	//Resource
	public MutableLiveData<String> payment = new MutableLiveData<>("");
	public MutableLiveData<String> workingTime = new MutableLiveData<>("");
	public MutableLiveData<String> workingDay = new MutableLiveData<>("");
	public MutableLiveData<String> weeklySalary = new MutableLiveData<>("");
	public MutableLiveData<String> salary = new MutableLiveData<>("");
	public MutableLiveData<String> insurance = new MutableLiveData<>("");
	public MutableLiveData<String> weeklyAllowance = new MutableLiveData<>("");
	public MutableLiveData<String> expectedTotalSalary = new MutableLiveData<>();

	public MutableLiveData<Boolean> insuranceSwitch = new MutableLiveData<>(true);
	public MutableLiveData<Boolean> weeklyAllowanceSwitch = new MutableLiveData<>(true);

	public ExpectedPayViewModel(@NonNull Application application) {
		super(application);
	}

	@Override
	protected void onResume(Activity activity) {
		super.onResume(activity);

	}

	public void calculate(){
		if(!payment.getValue().equals("") && !workingDay.getValue().equals("")) {
			int _payment = Integer.parseInt(payment.getValue());
			double _workingTime;
			if(workingTime.getValue().equals("")) {
				_workingTime = 1;
			}
			else {
				_workingTime = Double.parseDouble(workingTime.getValue());
			}
			int _workingDay = Integer.parseInt(workingDay.getValue());
			double _weeklySalary = (_payment * _workingTime) * _workingDay;
			double _salary = _weeklySalary * 4;

			//4대 보험 계산
			int _insurance;
			if(insuranceSwitch.getValue()) {
				_insurance = (int) (_salary * 0.033);
			}
			else {
				_insurance = 0;
			}
			//주휴수당 계산
			double _weeklyAllowance;
			if(weeklyAllowanceSwitch.getValue()) {

				if (_workingTime * _workingDay >= 40) {
					_weeklyAllowance = 8 * _payment;
				} else if (_workingTime * _workingDay >= 15) {
					_weeklyAllowance = ((_workingTime * _workingDay) / 40.0) * 8 * _payment;
				} else {
					_weeklyAllowance = 0;
				}
			}
			else {
				_weeklyAllowance = 0;
			}

			//실 월급 계산
			double _expectedSalary = _salary;
			_expectedSalary -= _insurance;
			_expectedSalary += _weeklyAllowance * 4;
//			if(insuranceSwitch.getValue()) {
//			}
//			if(weeklyAllowanceSwitch.getValue()){
//			}

			weeklySalary.setValue(StringUtil.addComma(_weeklySalary) + activity.getResources().getString(R.string.won));
			salary.setValue(StringUtil.addComma(_salary) + activity.getResources().getString(R.string.won));
			if(_insurance == 0){
				insurance.setValue(StringUtil.addComma(_insurance) + activity.getResources().getString(R.string.won));
			}
			else {
				insurance.setValue("- " + StringUtil.addComma(_insurance) + activity.getResources().getString(R.string.won));
			}
			if(_weeklyAllowance == 0){
				weeklyAllowance.setValue(StringUtil.addComma(_weeklyAllowance) + activity.getResources().getString(R.string.won));
			}
			else {
				weeklyAllowance.setValue("+ " + StringUtil.addComma(_weeklyAllowance) + activity.getResources().getString(R.string.won));
			}
			expectedTotalSalary.setValue(StringUtil.addComma(_expectedSalary) + activity.getResources().getString(R.string.won));
		}
	}
}