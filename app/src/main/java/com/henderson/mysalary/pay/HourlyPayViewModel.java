package com.henderson.mysalary.pay;

import android.app.Activity;
import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.henderson.mysalary.BaseViewModel;
import com.henderson.mysalary.dialog.ManageJobDialog;
import com.henderson.mysalary.model.DailyPay;
import com.henderson.mysalary.model.Job;
import com.henderson.mysalary.model.Month;
import com.henderson.mysalary.model.SalaryInfo;
import com.henderson.mysalary.utils.db.AppDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HourlyPayViewModel extends BaseViewModel implements ManageJobDialog.UpdateJobDialogListener {
	public HourlyPayViewModel(@NonNull Application application) {
		super(application);
	}

	public PayListener listener;

	public MutableLiveData<String> currentYear = new MutableLiveData<>();
	public MutableLiveData<String> currentMonth = new MutableLiveData<>();
	public MutableLiveData<Boolean> isNextMonthSalary = new MutableLiveData<>(false);
	protected Month thisMonth;
	protected Month prevMonth;
	protected ArrayList<SalaryInfo> salaryInfoList;

	@Override
	protected void onCreate(Activity activity) {
		this.activity = activity;
		setMonth();
		setSalaryInfo();
	}

	private void setMonth() {
		//Set this month
		thisMonth = new Month();
		prevMonth = new Month();
		try {
			Date currentDate = Calendar.getInstance().getTime();
			SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
			SimpleDateFormat monthFormat = new SimpleDateFormat("M");
			currentYear.setValue(yearFormat.format(currentDate));
			currentMonth.setValue(monthFormat.format(currentDate));

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentDate);
			calendar.set(Calendar.DATE, 1);

			thisMonth.setYear(Integer.parseInt(yearFormat.format(currentDate)));
			thisMonth.setMonth(Integer.parseInt(monthFormat.format(currentDate)));
			thisMonth.setLastDate(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			thisMonth.setDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK));

			if (Integer.parseInt(monthFormat.format(currentDate)) - 1 == 0) {
				prevMonth.setYear(Integer.parseInt(yearFormat.format(currentDate)) - 1);
				prevMonth.setMonth(12);
			} else {
				prevMonth.setYear(Integer.parseInt(yearFormat.format(currentDate)));
				prevMonth.setMonth(Integer.parseInt(monthFormat.format(currentDate)) - 1);
			}

			setJobData();

		} catch (Exception e) {
			log("setMonthlyData", "exception " + e);
		}
	}

	private void updateMonth(int newYear, int newMonth) {
		isNextMonthSalary.setValue(false);
		thisMonth = new Month();
		try {
			currentYear.setValue(String.valueOf(newYear));
			currentMonth.setValue(String.valueOf(newMonth));

			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, newYear);
			calendar.set(Calendar.MONTH, newMonth - 1);
			calendar.set(Calendar.DATE, 1);

			thisMonth.setYear(newYear);
			thisMonth.setMonth(newMonth);
			thisMonth.setLastDate(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			thisMonth.setDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK));

			if (newMonth - 1 == 0) {
				prevMonth.setYear(newYear - 1);
				prevMonth.setMonth(12);
			} else {
				prevMonth.setYear(newYear);
				prevMonth.setMonth(newMonth - 1);
			}
			setJobData();
			setSalaryInfo();
		} catch (Exception e) {
			log("setMonthlyData", "exception " + e);
		}
	}

	protected void setJobData() {
		thisMonth.setJobList(AppDatabase.getAppDatabase(getApplication()).getJobList());
		for (int count = 0; count < thisMonth.getJobList().size(); count++) {
			List<DailyPay> dailyPayList = AppDatabase.getAppDatabase(getApplication()).getDailyPay(0, thisMonth.getJobList().get(count).jobName, thisMonth.getYear(), thisMonth.getMonth());
			thisMonth.getJobList().get(count).setDailyPayList(dailyPayList);
		}

		prevMonth.setJobList(AppDatabase.getAppDatabase(getApplication()).getJobList());
		for (int count = 0; count < prevMonth.getJobList().size(); count++) {
			List<DailyPay> dailyPayList = AppDatabase.getAppDatabase(getApplication()).getDailyPay(0, prevMonth.getJobList().get(count).jobName, prevMonth.getYear(), prevMonth.getMonth());
			prevMonth.getJobList().get(count).setDailyPayList(dailyPayList);
		}

		listener.setCalendar(thisMonth);
	}

	protected void setSalaryInfo() {
		salaryInfoList = new ArrayList<>();

		//First, calculate dailypay of this month.
		for (int i = 0; i < thisMonth.getJobList().size(); i++) {
			Job _job = thisMonth.getJobList().get(i);

			SalaryInfo salaryInfo = new SalaryInfo();
			salaryInfo.jobName = _job.jobName;
			salaryInfo.jobColor = _job.jobColor;
			salaryInfo.payday = _job.payday;
			salaryInfo.thisYear = this.thisMonth.getYear();
			salaryInfo.thisMonth = this.thisMonth.getMonth();
			salaryInfo.prevYear = this.prevMonth.getYear();
			salaryInfo.prevMonth = this.prevMonth.getMonth();

			for (int j = 0; j < _job.getDailyPayList().size(); j++) {
				DailyPay _dailyPay = _job.getDailyPayList().get(j);

				if (_dailyPay.date < _job.payday) {
					salaryInfo.salary += _dailyPay.hourlyPayment * _dailyPay.workingTime / 60;
				}
				else {
					isNextMonthSalary.setValue(true);
				}
			}

			if (salaryInfo.salary != 0) salaryInfoList.add(salaryInfo);
		}

		//Second, calculate dailypay of prev month.
		for (int i = 0; i < prevMonth.getJobList().size(); i++) {
			Job _job = prevMonth.getJobList().get(i);

			for (int j = 0; j < _job.getDailyPayList().size(); j++) {
				DailyPay _dailyPay = _job.getDailyPayList().get(j);
				if (_dailyPay.date >= _job.payday) {
					//전 달에서 데이터 안채워서 비어있는 경우 무조건 추가
					if (salaryInfoList.isEmpty()) {
						SalaryInfo salaryInfo = new SalaryInfo();
						salaryInfo.jobName = _job.jobName;
						salaryInfo.jobColor = _job.jobColor;
						salaryInfo.payday = _job.payday;
						salaryInfo.thisYear = this.thisMonth.getYear();
						salaryInfo.thisMonth = this.thisMonth.getMonth();
						salaryInfo.prevYear = this.prevMonth.getYear();
						salaryInfo.prevMonth = this.prevMonth.getMonth();
						salaryInfoList.add(salaryInfo);
					}

					for (int z = 0; z < salaryInfoList.size(); z++) {
						if (salaryInfoList.get(z).jobName.equals(_job.jobName)) {
							salaryInfoList.get(z).salary += _dailyPay.hourlyPayment * _dailyPay.workingTime / 60;
							break;
						} else if (z == salaryInfoList.size() - 1) {
							SalaryInfo salaryInfo = new SalaryInfo();
							salaryInfo.jobName = _job.jobName;
							salaryInfo.jobColor = _job.jobColor;
							salaryInfo.salary += _dailyPay.hourlyPayment * _dailyPay.workingTime / 60;
							salaryInfo.payday = _job.payday;
							salaryInfo.thisYear = this.thisMonth.getYear();
							salaryInfo.thisMonth = this.thisMonth.getMonth();
							salaryInfo.prevYear = this.prevMonth.getYear();
							salaryInfo.prevMonth = this.prevMonth.getMonth();
							salaryInfoList.add(salaryInfo);
							break;
						}
					}
				}
			}
		}
		listener.setPaydayInfo(salaryInfoList);
	}

	protected void addDailyPay(DailyPay dailyPay) {
		//TODO: check data is existing
		AppDatabase.getAppDatabase(getApplication()).insertDailyPay(dailyPay);
	}

	public void clickManageJob(View view) {
		ManageJobDialog dialog = new ManageJobDialog(activity);
		dialog.updateJobDialogListener = this;
		dialog.show();
	}

//	public void clickAddJob(View view) {
//		AddJobDialog dialog = new AddJobDialog(activity);
//		dialog.show();
//	}
//
//	public void clickUpdateJob(View view) {
//		if (checkJob()) {
//			toast(activity, R.string.toast_empty_job);
//		} else {
//			UpdateJobDialog dialog = new UpdateJobDialog(activity);
//			dialog.updateJobDialogListener = this;
//			dialog.show();
//		}
//	}

	public void clickPrev(){
		int _currentYear = Integer.parseInt(currentYear.getValue());
		int _currentMonth = Integer.parseInt(currentMonth.getValue());
		int prevYear;
		int prevMonth;

		if (_currentMonth <= 1) {
			prevYear = _currentYear - 1;
			prevMonth = 12;
		} else {
			prevYear = _currentYear;
			prevMonth = _currentMonth - 1;
		}

		updateMonth(prevYear, prevMonth);
	}

	public void clickPrev(View view) {
		clickPrev();
	}

	public void clickNext(){
		int _currentYear = Integer.parseInt(currentYear.getValue());
		int _currentMonth = Integer.parseInt(currentMonth.getValue());
		int prevYear;
		int prevMonth;

		if (_currentMonth >= 12) {
			prevYear = _currentYear + 1;
			prevMonth = 1;
		} else {
			prevYear = _currentYear;
			prevMonth = _currentMonth + 1;
		}

		updateMonth(prevYear, prevMonth);
	}
	public void clickNext(View view) {
		clickNext();
	}

	@Override
	public void finishUpdate() {
		setJobData();
		setSalaryInfo();
	}
}
