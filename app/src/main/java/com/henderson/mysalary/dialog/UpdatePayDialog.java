package com.henderson.mysalary.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.henderson.mysalary.R;
import com.henderson.mysalary.model.DailyPay;
import com.henderson.mysalary.utils.StringUtil;
import com.henderson.mysalary.utils.db.AppDatabase;

import static com.henderson.mysalary.utils.BaseUtil.toast;

//This dialog is implemented MVC, not MVVM
public class UpdatePayDialog extends Dialog {
	public enum Type {
		HOURLY, DAILY
	}

	private Context context;
	public InputPayDialogListener listener;

	//Resource variable
	private TextView dialogTitle;
	private TextView payment;
	private EditText paymentData;
	private TextView workingTime;
	private EditText hourData;
	private EditText minuteData;
	private Button positiveButton;

	private DailyPay dailyPay;
	private Type type;

	public UpdatePayDialog(@NonNull Context context, DailyPay dailyPay, Type type) {
		super(context);
		this.dailyPay = new DailyPay();
		this.type = type;
		this.context = context;
		this.dailyPay = dailyPay;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initView();
		setView();
	}

	private void initView() {
		setContentView(R.layout.dialog_update_pay);

		Window window = this.getWindow();
		window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		//Set resource variable
		dialogTitle = findViewById(R.id.update_pay_title);
		payment = findViewById(R.id.update_pay_payment);
		paymentData = findViewById(R.id.update_pay_payment_data);
		workingTime = findViewById(R.id.update_pay_working_time);
		hourData = findViewById(R.id.update_pay_hour_data);
//		hourText = findViewById(R.id.update_pay_hour);
		minuteData = findViewById(R.id.update_pay_minute_data);
//		minuteText = findViewById(R.id.update_pay_minute);
		positiveButton = findViewById(R.id.update_pay_positive_button);

		//Set Text
		switch (type) {
			case HOURLY:
				dialogTitle.setText(R.string.dialog_input_pay_dailypay_title_job_list);
				payment.setText(R.string.dialog_input_pay_hourlypay);
				workingTime.setVisibility(View.VISIBLE);
				break;
			case DAILY:
				dialogTitle.setText(R.string.dialog_input_pay_dailypay_title_job_list);
				payment.setText(R.string.dialog_input_pay_dailypay);
				workingTime.setVisibility(View.GONE);
				break;
		}

		findViewById(R.id.update_pay_negative_button).setOnClickListener((View view) -> {
			dismiss();
		});

		positiveButton.setOnClickListener((View view) -> {
			if (checkInputData()) {
				updateData();
			}
		});
	}

	private void setView() {
		switch (type) {
			case HOURLY:
				paymentData.setText("" + dailyPay.hourlyPayment);
				hourData.setText("" + dailyPay.workingTime / 60);
				minuteData.setText("" + (dailyPay.workingTime - ((dailyPay.workingTime / 60) * 60)));
				break;
			case DAILY:
				paymentData.setText("" + dailyPay.dailyPayment);
				break;
		}
	}

	private boolean checkInputData() {
		if (!StringUtil.checkStringWithZero(paymentData)) {
			switch (type) {
				case HOURLY:
					toast(context, R.string.toast_empty_hourly_pay);
					break;
				case DAILY:
					toast(context, R.string.toast_empty_daily_pay);
					break;
			}
			return false;
		} else if (this.type == Type.HOURLY && !StringUtil.checkStringWithZero(hourData)) {
			toast(context, R.string.toast_empty_working_time);
			return false;
		}
		return true;
	}

	private void updateData() {
		setDailyPay(calculateWorkingTime());
		AppDatabase.getAppDatabase(context).updateDailyPay(this.dailyPay);
		toast(context, R.string.toast_success_update);
		listener.closeDialog();
		dismiss();
	}

	private int calculateWorkingTime() {
		if (!StringUtil.checkString(hourData)) {
			return 0;
		}
		int _hour = Integer.parseInt(hourData.getText().toString());
		int _minute = StringUtil.checkString(minuteData) ? Integer.parseInt(minuteData.getText().toString()) : 0;

		return (_hour * 60) + _minute;
	}

	private void setDailyPay(int workingTime) {
		switch (type) {
			case HOURLY:
				this.dailyPay.salaryType = 0;
				this.dailyPay.hourlyPayment = Integer.parseInt(paymentData.getText().toString());
				this.dailyPay.workingTime = workingTime;
				break;
			case DAILY:
				this.dailyPay.salaryType = 1;
				this.dailyPay.dailyPayment = Integer.parseInt(paymentData.getText().toString());
				break;
		}
	}

	public interface InputPayDialogListener {
		void closeDialog();
	}
}
