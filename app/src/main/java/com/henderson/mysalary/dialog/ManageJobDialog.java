package com.henderson.mysalary.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.henderson.mysalary.R;
import com.henderson.mysalary.model.Job;
import com.henderson.mysalary.utils.StringUtil;
import com.henderson.mysalary.utils.db.AppDatabase;

import java.util.ArrayList;
import java.util.List;

import static com.henderson.mysalary.utils.BaseUtil.toast;

public class ManageJobDialog extends Dialog {

	public UpdateJobDialogListener updateJobDialogListener;

	private JobIndicatorAdapter jobIndicatorAdapter;

	private Context context;
	private List<Job> jobList;

	//Resource variable
	private RadioGroup radioGroup;
	private RadioButton addRadioButton;
	private RadioButton updateRadioButton;
	private TextInputLayout jobNameLayout;
	private EditText jobNameText;
	private TextInputLayout jobPaymentLayout;
	private EditText jobPaymentText;
	private TextView oldJobText;
	private Spinner oldJobSpinner;
	private TextInputLayout oldJobPaymentLayout;
	private EditText oldJobPaymentText;
	private RecyclerView jobIndicator;
	private Spinner paydaySpinner;
	private Button updateButton;
	private Button positiveButton;

	private int oldJobIndex = 0;
	private int paydayIndex = 0;

	public ManageJobDialog(@NonNull Context context) {
		super(context);
		this.context = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		jobList = AppDatabase.getAppDatabase(context).getJobList();

		initView();
		setView();
	}

	private void initView() {
		setContentView(R.layout.dialog_manage_job);

		Window window = this.getWindow();
		window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		//Set resource variable
		radioGroup = findViewById(R.id.manage_job_radio);
		addRadioButton = findViewById(R.id.manage_job_radio_add);
		updateRadioButton = findViewById(R.id.manage_job_radio_update);
		jobNameLayout = findViewById(R.id.manage_job_name_layout);
		jobNameText = findViewById(R.id.manage_job_name);
		jobPaymentLayout = findViewById(R.id.manage_job_payment_layout);
		jobPaymentText = findViewById(R.id.manage_job_payment);
		oldJobText = findViewById(R.id.manage_job_old);
		oldJobPaymentLayout = findViewById(R.id.manage_old_job_payment_layout);
		oldJobPaymentText = findViewById(R.id.manage_old_job_payment);
		oldJobSpinner = findViewById(R.id.manage_job_old_spinner);
		jobIndicator = findViewById(R.id.manage_job_indicator);
		paydaySpinner = findViewById(R.id.manage_job_payday_spinner);
		updateButton = findViewById(R.id.manage_job_update_button);
		positiveButton = findViewById(R.id.manage_job_positive_button);

		//Set JobIndicator RecyclerView
		LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
		jobIndicator.setLayoutManager(layoutManager);

		jobIndicatorAdapter = new JobIndicatorAdapter();
		jobIndicator.setAdapter(jobIndicatorAdapter);

		//Set Listener
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				setView();
			}
		});

		//Set Payday Spinner
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.job_payday_spinner, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		paydaySpinner.setAdapter(adapter);

		paydaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				paydayIndex = i;
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
			}
		});

		findViewById(R.id.manage_job_negative_button).setOnClickListener((View view) -> {
			dismiss();
		});

		updateButton.setOnClickListener((View view) -> {
			updateJob();
			updateJobDialogListener.finishUpdate();
			dismiss();
		});

		positiveButton.setOnClickListener((View view) -> {
			if (addRadioButton.isChecked()) {
				checkInputData();
			} else {
				AlertDialog.Builder dialog = new AlertDialog.Builder(context);
				dialog.setMessage(R.string.delete_job_message);
				dialog.setPositiveButton(R.string.delete, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						AppDatabase.getAppDatabase(context).deleteJob(jobList.get(oldJobIndex));
						updateJobDialogListener.finishUpdate();
						toast(context, R.string.toast_success_delete);
						dismiss();
					}
				});
				dialog.setNegativeButton(R.string.cancel, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dialog.show();			}
		});
	}

	private void setView() {
		if (jobList.size() == 0) {
			updateRadioButton.setEnabled(false);
		} else {
			updateRadioButton.setEnabled(true);
		}

		if (addRadioButton.isChecked()) {
			jobNameLayout.setVisibility(View.VISIBLE);
			jobPaymentLayout.setVisibility(View.VISIBLE);
			oldJobText.setVisibility(View.GONE);
			oldJobPaymentLayout.setVisibility(View.GONE);
			oldJobSpinner.setVisibility(View.GONE);
			updateButton.setVisibility(View.GONE);
			positiveButton.setText(R.string.add);
			positiveButton.setTextColor(getContext().getColor(R.color.corn_flower_blue));

			//Init index
			jobIndicatorAdapter.initColor();
			jobIndicatorAdapter.notifyDataSetChanged();

			paydayIndex = 0;
			paydaySpinner.setSelection(paydayIndex);

		} else {
			jobNameLayout.setVisibility(View.GONE);
			jobPaymentLayout.setVisibility(View.GONE);
			oldJobText.setVisibility(View.VISIBLE);
			oldJobPaymentLayout.setVisibility(View.VISIBLE);
			oldJobSpinner.setVisibility(View.VISIBLE);
			updateButton.setVisibility(View.VISIBLE);
			positiveButton.setText(R.string.delete);
			positiveButton.setTextColor(getContext().getColor(R.color.red));

			List<String> jobNameList = new ArrayList<>();
			for (int i = 0; i < jobList.size(); i++) {
				jobNameList.add(jobList.get(i).jobName);
			}

			//Set Job Spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, jobNameList);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			oldJobSpinner.setAdapter(adapter);

			oldJobSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
					oldJobIndex = i;
					jobIndicatorAdapter.setColor(jobList.get(oldJobIndex).jobColor);
					jobIndicatorAdapter.notifyDataSetChanged();
					oldJobPaymentText.setText(""+jobList.get(oldJobIndex).payment);

					paydayIndex = jobList.get(oldJobIndex).payday - 1;
					paydaySpinner.setSelection(paydayIndex);
				}

				@Override
				public void onNothingSelected(AdapterView<?> adapterView) {
				}
			});
		}
	}

	private void checkInputData() {
		if (!StringUtil.checkString(jobNameText)) {
			toast(context, R.string.toast_empty_job_name);
		}
		else if (!StringUtil.checkString(jobPaymentText)){
			toast(context, R.string.toast_empty_pay);
		}
		else {
			insertJobIntoDB();
			dismiss();
		}
	}

	private void insertJobIntoDB() {
		Job job = new Job();
		job.jobName = jobNameText.getText().toString();
		job.jobColor = jobIndicatorAdapter.getColor();
		job.payday = paydayIndex+1;
		job.payment = Integer.parseInt(jobPaymentText.getText().toString());
		int checkEmpty = AppDatabase.getAppDatabase(getContext()).checkJobName(job.jobName);
		if (checkEmpty == 0) {
			AppDatabase.getAppDatabase(getContext()).insertJob(job);
			toast(context, R.string.toast_success_add_job);
		} else {
			toast(context, R.string.toast_already_job);
		}
	}

	private void updateJob(){
		jobList.get(oldJobIndex).jobColor = jobIndicatorAdapter.getColor();
		jobList.get(oldJobIndex).payment = Integer.parseInt(oldJobPaymentText.getText().toString());
		jobList.get(oldJobIndex).payday = paydayIndex + 1;
		AppDatabase.getAppDatabase(context).updateJob(jobList.get(oldJobIndex));

		toast(context, R.string.toast_success_update);
	}

	private class JobIndicatorAdapter extends RecyclerView.Adapter<JobIndicatorAdapter.ViewHolder> {
//		private String[] colorList = {"tomato", "orange_", "yellow_", "light_green", "turquoise_", "deep_sky_blue", "medium_blue", "navy", "blueblack_"};
		private String[] colorList = {"color0", "color1", "color2", "color3", "color4", "color5", "color6", "color7"};
		private int selectedItem;

		public class ViewHolder extends RecyclerView.ViewHolder {
			ImageView indicator;
			ImageView checker;

			ViewHolder(View itemView) {
				super(itemView);
				indicator = itemView.findViewById(R.id.job_indicator_indicator);
				checker = itemView.findViewById(R.id.job_indicator_checker);
				itemView.setOnClickListener((View view) -> {
					selectedItem = getAdapterPosition();
					notifyDataSetChanged();
				});
			}
		}

		private void initColor(){
			selectedItem = 0;
			jobIndicator.smoothScrollToPosition(selectedItem);
		}

		private void setColor(String color){
			for(int i=0; i<colorList.length; i++){
				if(color.equals(colorList[i])) {
					this.selectedItem = i;
					break;
				}
			}
			jobIndicator.smoothScrollToPosition(selectedItem);
		}

		public String getColor(){
			return colorList[selectedItem];
		}

		@NonNull
		@Override
		public JobIndicatorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View view = inflater.inflate(R.layout.item_job_indicator, parent, false);
			JobIndicatorAdapter.ViewHolder viewHolder = new JobIndicatorAdapter.ViewHolder(view);

			return viewHolder;
		}

		@Override
		public void onBindViewHolder(@NonNull JobIndicatorAdapter.ViewHolder holder, int position) {
			GradientDrawable shape = (GradientDrawable) holder.indicator.getDrawable();
			switch (colorList[position]) {
				case "tomato":
					shape.setColor(getContext().getColor(R.color.tomato));
					break;
				case "coral":
					shape.setColor(getContext().getColor(R.color.coral));
					break;
				case "salmon":
					shape.setColor(getContext().getColor(R.color.salmon));
					break;
				case "orange":
					shape.setColor(getContext().getColor(R.color.orange));
					break;
				case "light_green":
					shape.setColor(getContext().getColor(R.color.light_green));
					break;
				case "corn_flower_blue":
					shape.setColor(getContext().getColor(R.color.corn_flower_blue));
					break;
				case "deep_sky_blue":
					shape.setColor(getContext().getColor(R.color.deep_sky_blue));
					break;
				case "dodger_blue":
					shape.setColor(getContext().getColor(R.color.dodger_blue));
					break;
				case "medium_blue":
					shape.setColor(getContext().getColor(R.color.medium_blue));
					break;
				case "navy":
					shape.setColor(getContext().getColor(R.color.navy));
					break;
//				case "orange_":
//					shape.setColor(getContext().getColor(R.color.orange_));
//					break;
//				case "yellow_":
//					shape.setColor(getContext().getColor(R.color.yellow_));
//					break;
//				case "turquoise_":
//					shape.setColor(getContext().getColor(R.color.turquoise_));
//					break;
//				case "blueblack_":
//					shape.setColor(getContext().getColor(R.color.blueblack_));
//					break;
				case "color0":
					shape.setColor(getContext().getColor(R.color.color0));
					break;
				case "color1":
					shape.setColor(getContext().getColor(R.color.color1));
					break;
				case "color2":
					shape.setColor(getContext().getColor(R.color.color2));
					break;
				case "color3":
					shape.setColor(getContext().getColor(R.color.color3));
					break;
				case "color4":
					shape.setColor(getContext().getColor(R.color.color4));
					break;
				case "color5":
					shape.setColor(getContext().getColor(R.color.color5));
					break;
				case "color6":
					shape.setColor(getContext().getColor(R.color.color6));
					break;
				case "color7":
					shape.setColor(getContext().getColor(R.color.color7));
					break;
				default:
					break;
			}

			if(position == selectedItem) {
				holder.checker.setBackgroundResource(R.drawable.ic_check);

			}
			else {
				holder.checker.setBackgroundResource(0);
			}
		}

		@Override
		public int getItemCount() {
			return colorList.length;
		}
	}

	public interface UpdateJobDialogListener {
		void finishUpdate();
	}
}
