package com.henderson.mysalary.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.henderson.mysalary.R;
import com.henderson.mysalary.model.DailyPay;
import com.henderson.mysalary.utils.Constant;
import com.henderson.mysalary.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class SalaryWorkListAdapter extends RecyclerView.Adapter<SalaryWorkListAdapter.ViewHolder> {

	private Context context;
	private Constant.SalaryType salaryType;
	private List<DailyPay> workList = new ArrayList<>();

	public SalaryWorkListListener salaryWorkListListener;

	public SalaryWorkListAdapter(Context context, Constant.SalaryType salaryType) {
		this.context = context;
		this.salaryType = salaryType;
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		TextView date;
		TextView pay;
		TextView workingTime;
		ImageView edit;
		ImageView delete;

		ViewHolder(View itemView) {
			super(itemView);
			date = itemView.findViewById(R.id.work_list_date);
			pay = itemView.findViewById(R.id.work_list_pay);
			workingTime = itemView.findViewById(R.id.work_list_working_time);
			edit = itemView.findViewById(R.id.work_list_edit);
			delete = itemView.findViewById(R.id.work_list_delete);

			switch (salaryType) {
				case HOURLY_PAY:
					pay.setText(R.string.hourlypay);
					workingTime.setVisibility(View.VISIBLE);
					break;
				case DAILY_PAY:
					pay.setText(R.string.dailypay);
					workingTime.setVisibility(View.GONE);
					break;
			}

			edit.setOnClickListener(view -> {
				salaryWorkListListener.clickEdit(workList.get(getAdapterPosition()));
			});

			delete.setOnClickListener(view -> {
				salaryWorkListListener.clickDelete(workList.get(getAdapterPosition()).id);
			});
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View view = inflater.inflate(R.layout.item_work_list, parent, false);
		ViewHolder viewHolder = new ViewHolder(view);

		return viewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		holder.date.setText(workList.get(position).month + "/" + workList.get(position).date);

		switch (salaryType) {
			case HOURLY_PAY:
				holder.pay.setText(StringUtil.addComma(workList.get(position).hourlyPayment)+ context.getResources().getString(R.string.won));
				holder.workingTime.setText(workList.get(position).workingTime/60.0 + context.getResources().getString(R.string.hour));
				break;
			case DAILY_PAY:
				holder.pay.setText(StringUtil.addComma(workList.get(position).dailyPayment) + context.getResources().getString(R.string.won));
				break;
		}
	}

	@Override
	public int getItemCount() {
		return workList.size();
	}

	public void setWorkList(List<DailyPay> workList){
		this.workList = workList;
	}

	public interface SalaryWorkListListener{
		void clickEdit(DailyPay dailypay);
		void clickDelete(int id);
	}
}
