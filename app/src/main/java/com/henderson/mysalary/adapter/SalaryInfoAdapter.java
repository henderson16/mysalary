package com.henderson.mysalary.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.henderson.mysalary.R;
import com.henderson.mysalary.model.SalaryInfo;
import com.henderson.mysalary.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class SalaryInfoAdapter extends RecyclerView.Adapter<SalaryInfoAdapter.ViewHolder> {

	private Context context;
	private List<SalaryInfo> paydayList = new ArrayList<>();
	public SalaryInfoListener listener;

	public SalaryInfoAdapter(Context context) {
		this.context = context;
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		TextView period;
		ImageView indicator;
		TextView jobName;
		TextView salary;

		ViewHolder(View itemView) {
			super(itemView);
			period = itemView.findViewById(R.id.salary_info_period);
			indicator = itemView.findViewById(R.id.salary_info_indicator);
			jobName = itemView.findViewById(R.id.salary_info_job_name);
			salary = itemView.findViewById(R.id.salary_info_salary_data);

			itemView.setOnClickListener((View view) -> {
				listener.clickSalary(paydayList.get(getAdapterPosition()).jobName);
			});
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View view = inflater.inflate(R.layout.item_salary_info, parent, false);
		ViewHolder viewHolder = new ViewHolder(view);

		return viewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		SalaryInfo salaryInfo = paydayList.get(position);
		holder.period.setText(salaryInfo.jobName);

		if (salaryInfo.payday - 1 == 0) {
			holder.period.setText(salaryInfo.prevMonth + "월 " + salaryInfo.payday+ "일 ~ " + salaryInfo.prevMonth + "월 " + "말일");
		} else {
			holder.period.setText(salaryInfo.prevMonth + "월" + salaryInfo.payday + "일 ~ " + salaryInfo.thisMonth + "월" + (salaryInfo.payday - 1) + "일");
		}

		GradientDrawable shape = (GradientDrawable) holder.indicator.getDrawable();
		switch (salaryInfo.jobColor) {
			case "tomato":
				shape.setColor(context.getColor(R.color.tomato));
				break;
			case "coral":
				shape.setColor(context.getColor(R.color.coral));
				break;
			case "salmon":
				shape.setColor(context.getColor(R.color.salmon));
				break;
			case "orange":
				shape.setColor(context.getColor(R.color.orange));
				break;
			case "light_green":
				shape.setColor(context.getColor(R.color.light_green));
				break;
			case "corn_flower_blue":
				shape.setColor(context.getColor(R.color.corn_flower_blue));
				break;
			case "deep_sky_blue":
				shape.setColor(context.getColor(R.color.deep_sky_blue));
				break;
			case "dodger_blue":
				shape.setColor(context.getColor(R.color.dodger_blue));
				break;
			case "medium_blue":
				shape.setColor(context.getColor(R.color.medium_blue));
				break;
			case "navy":
				shape.setColor(context.getColor(R.color.navy));
				break;
//			case "orange_":
//				shape.setColor(context.getColor(R.color.orange_));
//				break;
//			case "yellow_":
//				shape.setColor(context.getColor(R.color.yellow_));
//				break;
//			case "turquoise_":
//				shape.setColor(context.getColor(R.color.turquoise_));
//				break;
//			case "blueblack_":
//				shape.setColor(context.getColor(R.color.blueblack_));
//				break;
			case "color0":
				shape.setColor(context.getColor(R.color.color0));
				break;
			case "color1":
				shape.setColor(context.getColor(R.color.color1));
				break;
			case "color2":
				shape.setColor(context.getColor(R.color.color2));
				break;
			case "color3":
				shape.setColor(context.getColor(R.color.color3));
				break;
			case "color4":
				shape.setColor(context.getColor(R.color.color4));
				break;
			case "color5":
				shape.setColor(context.getColor(R.color.color5));
				break;
			case "color6":
				shape.setColor(context.getColor(R.color.color6));
				break;
			case "color7":
				shape.setColor(context.getColor(R.color.color7));
				break;
			default:
				break;
		}
		holder.jobName.setText(salaryInfo.jobName);
		holder.salary.setText(StringUtil.addComma(salaryInfo.salary) + context.getResources().getString(R.string.won));
	}

	@Override
	public int getItemCount() {
		return paydayList.size();
	}

	public void setData(ArrayList<SalaryInfo> paydayList){
		this.paydayList = paydayList;
	}

	public interface SalaryInfoListener {
		void clickSalary(String jobName);
	}
}
