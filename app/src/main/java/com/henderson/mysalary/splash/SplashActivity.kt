package com.henderson.mysalary.splash

import android.app.AlertDialog
import com.henderson.mysalary.BaseActivity
import android.os.Bundle
import android.content.Intent
import android.net.Uri
import com.henderson.mysalary.R
import com.henderson.mysalary.main.MainActivity
import org.jsoup.Jsoup
import java.util.regex.Pattern

class SplashActivity : BaseActivity() {
    override fun onResume() {
        super.onResume()
        getVersionFromStore()
    }

    private fun getVersionFromStore() {
        Thread(Runnable {
            try {
                var version = ""
                val doc =
                    Jsoup.connect("https://play.google.com/store/apps/details?id=$packageName")
                        .get()
                val elements = doc.select(".htlgb")
                for (element in elements) {
                    version = element.text().trim { it <= ' ' }
                    if (Pattern.matches("^([0-9]|[1-9][0-9]).([0-9]|[1-9][0-9]).([0-9]|[1-9][0-9])$", version)) {
                        checkVersion(version)
                        return@Runnable
                    }
                }
                throw InterruptedException()
            } catch (e: Exception) {
                goToNext()
            }
        }).start()
    }

    private fun checkVersion(serverVersion: String) {
        runOnUiThread {
            val currentVersion = packageManager.getPackageInfo(packageName, 0).versionName

            val a = serverVersion.split(".")
            val b = currentVersion.split(".")

            if (a[0].toInt() > b[0].toInt()) {
                showUpdateDialog()
            } else if (a[0].toInt() == b[0].toInt()) {
                if (a[1].toInt() > b[1].toInt()) {
                    showUpdateDialog()
                } else if (a[1].toInt() == b[1].toInt()) {
                    if (a[2].toInt() > b[2].toInt()) {
                        showUpdateDialog()
                    } else if (a[2].toInt() == b[2].toInt()) {
                        goToNext()
                    } else {
                        goToNext()
                    }
                } else {
                    goToNext()
                }
            } else {
                goToNext()
            }
        }
    }


    private fun showUpdateDialog() {
        val dialog = AlertDialog.Builder(this)
        dialog.setMessage(R.string.update_message)
        dialog.setPositiveButton(R.string.ok) { dialog, which ->
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
        }

        dialog.setNegativeButton(R.string.cancel) { dialog, which ->
            goToNext()
        }

        dialog.setCancelable(false)
        dialog.show()
    }

    private fun goToNext() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}