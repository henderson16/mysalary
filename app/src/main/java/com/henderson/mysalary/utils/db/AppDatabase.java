package com.henderson.mysalary.utils.db;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.henderson.mysalary.model.DailyPay;
import com.henderson.mysalary.model.Job;

import java.util.List;

@Database(entities = {Job.class, DailyPay.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
	private static AppDatabase INSTANCE;

	final private int TEST = 0;
	final private int SELECT_CHECK_JOB = 11;
	final private int SELECT_CHECK_JOB_NAME = 12;
	final private int SELECT_GET_JOB_LIST = 13;
	final private int SELECT_GET_JOB = 14;
	final private int SELECT_GET_JOB_PAYMENT = 15;
	final private int SELECT_GET_JOB_PAYDAY = 16;
	final private int SELECT_GET_DAILYPAY = 17;
	final private int SELECT_GET_DAILYPAY_LIST = 18;
	final private int SELECT_GET_DAILYPAY_LIST_ORDER_BY = 19;
	final private int SELECT_GET_LAST_DAILYPAY = 20;
	final private int SELECT_GET_WORKING_LIST = 21;

	final private int INSERT_JOB = 31;
	final private int INSERT_DAILYPAY = 32;

	final private int DELETE_JOB = 41;
	final private int DELETE_DAILYPAY = 42;

	final private int UPDATE_JOB = 51;
	final private int UPDATE_DAILYPAY = 52;
//	final private int DELETE = 3;
//	final private int UPDATE = 4;

	static final Migration MIGRATION_1_2 = new Migration(1, 2) {
		@Override
		public void migrate(SupportSQLiteDatabase database) {
			database.execSQL("" +
					"ALTER TABLE tb_job ADD COLUMN payment INTEGER NOT NULL DEFAULT 0");
		}
	};


	public abstract DailyPayDao dailyPayDao();
	public abstract JobDao jobDao();

	public static AppDatabase getAppDatabase(Context context) {
		if(INSTANCE == null) {
			INSTANCE = Room.databaseBuilder(context, AppDatabase.class, "myapp-db")
					.addMigrations(MIGRATION_1_2)
					.build();
		}
		return INSTANCE;
	}

	public static void destroyInstance(){
		INSTANCE = null;
	}

	public int checkJobName(String jobName){
		try {
			return (int)new DBAsyncTask().execute(SELECT_CHECK_JOB_NAME, jobName).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return -1;
		}
	}

	public void insertJob(Job job){
		try {
			new DBAsyncTask().execute(INSERT_JOB, job).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
		}
	}

	public boolean updateJob(Job job){
		try {
			return (boolean)new DBAsyncTask().execute(UPDATE_JOB, job).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return false;
		}
	}

	public boolean deleteJob(Job job){
		try {
			return (boolean)new DBAsyncTask().execute(DELETE_JOB, job).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return false;
		}
	}

	public List<Job> getJobList(){
		try {
			return (List<Job>) new DBAsyncTask().execute(SELECT_GET_JOB_LIST).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return null;
		}
	}

	public Job getJob(String jobName){
		try {
			return (Job) new DBAsyncTask().execute(SELECT_GET_JOB, jobName).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return null;
		}
	}

	public int getJobPayment(String jobName){
		try {
			return (int) new DBAsyncTask().execute(SELECT_GET_JOB_PAYMENT, jobName).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return -1;
		}
	}

	public int getJobPayday(String jobName){
		try {
			return (int) new DBAsyncTask().execute(SELECT_GET_JOB_PAYDAY, jobName).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return -1;
		}
	}

	public void insertDailyPay(DailyPay dailyPay){
		try {
			new DBAsyncTask().execute(INSERT_DAILYPAY, dailyPay).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
		}
	}

	public DailyPay getDailyPay(int type, String jobName, int year, int month, int date) {
		try {
			return (DailyPay)new DBAsyncTask().execute(SELECT_GET_DAILYPAY, type, jobName, year, month, date).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return null;
		}
	}

	public List<DailyPay> getDailyPay(int type, String jobName, int year, int month){
		try {
			return (List<DailyPay>)new DBAsyncTask().execute(SELECT_GET_DAILYPAY_LIST, type, jobName, year, month).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return null;
		}
	}

	public List<DailyPay> getDailyPayOrderBy(int type, String jobName, int year, int month){
		try {
			return (List<DailyPay>)new DBAsyncTask().execute(SELECT_GET_DAILYPAY_LIST_ORDER_BY, type, jobName, year, month).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return null;
		}
	}

	public DailyPay getLastDailyPay(int type, String jobName){
		try {
			return (DailyPay) new DBAsyncTask().execute(SELECT_GET_LAST_DAILYPAY, type, jobName).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return null;
		}
	}

	public List<DailyPay> getWorkingList(int type, int year, int month, int date){
		try {
			return (List<DailyPay>) new DBAsyncTask().execute(SELECT_GET_WORKING_LIST, type, year, month, date).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return null;
		}
	}

	public boolean deleteDailyPay(int id){
		try {
			return (boolean)new DBAsyncTask().execute(DELETE_DAILYPAY, id).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return false;
		}
	}

	public boolean updateDailyPay(DailyPay dailyPay){
		try {
			return (boolean)new DBAsyncTask().execute(UPDATE_DAILYPAY, dailyPay).get();
		}
		catch (Exception e){
			Log.d("!@#", "exception : " + e);
			return false;
		}
	}

	/**
	 * @param DML
	 * @param object name
	 */
	class DBAsyncTask extends AsyncTask {
		@Override
		protected Object doInBackground(Object[] objects) {
			int dml = (int)objects[0];

			switch (dml) {
				case SELECT_CHECK_JOB_NAME:
					return jobDao().checkJobName((String)objects[1]);
				case SELECT_GET_JOB_LIST:
					return jobDao().getJobList();
				case SELECT_GET_JOB:
					return jobDao().getJob((String)objects[1]);
				case SELECT_GET_JOB_PAYMENT:
					return jobDao().getJobPayment((String)objects[1]);
				case SELECT_GET_JOB_PAYDAY:
					return jobDao().getJobPayday((String)objects[1]);
				case SELECT_GET_DAILYPAY:
					return dailyPayDao().getDailyPay((int)objects[1], (String)objects[2], (int)objects[3], (int)objects[4], (int)objects[5]);
				case SELECT_GET_DAILYPAY_LIST:
					return dailyPayDao().getDailyPayList((int)objects[1], (String)objects[2], (int)objects[3], (int)objects[4]);
				case SELECT_GET_DAILYPAY_LIST_ORDER_BY:
					return dailyPayDao().getDailyPayListOrderBy((int)objects[1], (String)objects[2], (int)objects[3], (int)objects[4]);
				case SELECT_GET_LAST_DAILYPAY:
					return dailyPayDao().getLastDailyPay((int)objects[1], (String)objects[2]);
				case SELECT_GET_WORKING_LIST:
					return dailyPayDao().getWorkingList((int)objects[1], (int)objects[2], (int)objects[3], (int)objects[4]);
				case INSERT_JOB:
					jobDao().insertJob((Job) objects[1]);
					break;
				case INSERT_DAILYPAY:
					dailyPayDao().insertDailyPay((DailyPay)objects[1]);
					break;
				case DELETE_JOB:
					jobDao().deleteJob((Job)objects[1]);
					return true;
				case DELETE_DAILYPAY:
					dailyPayDao().deleteDailyPay((int)objects[1]);
					return true;
				case UPDATE_JOB:
					jobDao().updateJob((Job)objects[1]);
					return true;
				case UPDATE_DAILYPAY:
					dailyPayDao().updateDailyPay((DailyPay)objects[1]);
					return true;
			}

			return null;
		}
	}
}