package com.henderson.mysalary.utils.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.henderson.mysalary.model.Job;

import java.util.List;

@Dao
public interface JobDao {
	@Query("SELECT * FROM tb_job")
	List<Job> getJobList();

	@Query("SELECT * FROM tb_job where name = :jobName")
	Job getJob(String jobName);

	@Query("SELECT payment FROM tb_job where name = :jobName")
	int getJobPayment(String jobName);

	@Query("SELECT COUNT(name) FROM tb_job where name = :jobName")
	int checkJobName(String jobName);

	@Query("SELECT payday FROM tb_job where name = :jobName")
	int getJobPayday(String jobName);

	@Insert
	void insertJob(Job job);

	@Delete()
	void deleteJob(Job job);

	@Update
	void updateJob(Job job);
}