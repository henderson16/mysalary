package com.henderson.mysalary.utils.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.henderson.mysalary.model.DailyPay;

import java.util.List;

@Dao
public interface DailyPayDao {
	@Query("SELECT * FROM tb_dailypay WHERE name = :jobName AND salary_type = :type AND year = :year AND month = :month AND date = :date")
	DailyPay getDailyPay(int type, String jobName, int year, int month, int date);

	@Query("SELECT * FROM tb_dailypay")
	List<DailyPay> getDailyPayList();

	@Query("SELECT * FROM tb_dailypay a inner join tb_job b ON a.name = b.name WHERE b.name = :jobName AND a.salary_type = :type AND a.year = :year AND a.month = :month")
	List<DailyPay> getDailyPayList(int type, String jobName, int year, int month);

	@Query("SELECT * FROM tb_dailypay a inner join tb_job b ON a.name = b.name WHERE b.name = :jobName AND a.salary_type = :type AND a.year = :year AND a.month = :month ORDER BY a.date ASC")
	List<DailyPay> getDailyPayListOrderBy(int type, String jobName, int year, int month);

	@Query("SELECT * FROM tb_dailypay WHERE name = :jobName AND salary_type = :type ORDER BY id DESC LIMIT 1")
	DailyPay getLastDailyPay(int type, String jobName);

	@Query("SELECT * FROM tb_dailypay WHERE salary_type = :type AND year = :year AND month = :month AND date = :date")
	List<DailyPay> getWorkingList(int type, int year, int month, int date);

	@Insert
	void insertDailyPay(DailyPay dailyPay);

	@Query("DELETE FROM tb_dailypay WHERE id = :id")
	void deleteDailyPay(int id);


	//	@Query("UPDATE tb_dailypay SET daily_payment = :dailyPayment WHERE id = :id")
	@Update
	void updateDailyPay(DailyPay dailyPay);
}
