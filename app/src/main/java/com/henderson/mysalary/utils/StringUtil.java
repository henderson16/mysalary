package com.henderson.mysalary.utils;

import android.widget.EditText;
import android.widget.TextView;

public class StringUtil {
	/**
	 * @param view
	 * @return if object is null, return false
	 */
	public static boolean checkString(EditText view){
		if(view.getText().toString() == null){
			return false;
		}
		else if(view.getText().toString().equals("")) {
			return false;
		}
		return true;
	}

	/**
	 * @param view
	 * @return if object is null, return false
	 */
	public static boolean checkStringWithZero(EditText view){
		if(view.getText().toString() == null){
			return false;
		}
		else if(view.getText().toString().equals("")) {
			return false;
		}
		else if(Integer.parseInt(view.getText().toString()) == 0) {
			return false;
		}
		return true;
	}

	/**
	 * @param view
	 * @return if object is null, return false
	 */
	public static boolean checkString(TextView view){
		if(view.getText().toString() == null){
			return false;
		}
		if(view.getText().toString().equals("")) {
			return false;
		}
		return true;
	}

	public static String addComma(int num){
		String number = String.valueOf(num);

		for(int i=number.length()-3; i>0; i-=3){
			number = number.substring(0, i) + "," + number.substring(i);
		}
		return number;
	}

	public static String addComma(double num){
		String number = String.valueOf((int)num);

		for(int i=number.length()-3; i>0; i-=3){
			number = number.substring(0, i) + "," + number.substring(i);
		}
		return number;
	}
}
