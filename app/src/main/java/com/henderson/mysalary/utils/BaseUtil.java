package com.henderson.mysalary.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import java.util.Calendar;

public class BaseUtil {

	static public AlertDialog.Builder showDialog(Context context, int msg, int okMsg){
		return showDialog(context, msg, okMsg, null, 0, null);
	}

	static public AlertDialog.Builder showDialog(Context context, int msg, int okMsg, DialogInterface.OnClickListener positiveEvent){
		return showDialog(context, msg, okMsg, positiveEvent, 0, null);
	}

	static public AlertDialog.Builder showDialog(Context context, int msg, int okMsg, DialogInterface.OnClickListener positiveEvent, int cancelMsg, DialogInterface.OnClickListener negativeEvent){
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setMessage(msg);
		if (okMsg != 0) {
			if (positiveEvent != null) dialog.setPositiveButton(okMsg, positiveEvent);
			else dialog.setPositiveButton(okMsg, null);
		}

		if (cancelMsg != 0) {
			if (negativeEvent != null) dialog.setNegativeButton(cancelMsg, negativeEvent);
			else dialog.setNegativeButton(cancelMsg, null);
		}

		return dialog;
	}

	static public void toast(Context context, String msg){
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	static public void toast(Context context, int msg){
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	static public int lastDateOfMonth(int year, int month){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month-1);
		return calendar.getActualMaximum(Calendar.DATE);
	}

	static public int dayOfWeek(int year, int month, int date){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.DATE, date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}
}