package com.henderson.mysalary.utils;

public class Constant {
	//For Debug
//	public final boolean D = true;
	//For Release
	public final boolean D = false;

	public final int REQ_SELECTED_GOODS = 10;

	public static final String REQUEST_APPROVAL = "request_approval";
	public static final String GOODS_LIST = "goods_list";
	public static final String HASH_MAP = "hash_map";
	public static final String JOBNAME = "job_name";
	public static final String JOBPAYDAY = "job_payday";
	public static final String THISYEAR = "this_year";
	public static final String THISMONTH = "this_month";
	public static final String THISMONTH_FIRST_DAY = "this_month_first_day";
	public static final String PREVYEAR = "prev_year";
	public static final String PREVMONTH = "prev_month";
	public static final String PREVMONTH_FIRST_DAY = "prev_month_first_day";
	public static final String SALARYTYPE = "salary_type";

	public enum SalaryType {
		HOURLY_PAY(0), DAILY_PAY(1);
		private int value;

		SalaryType(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}
}

