package com.henderson.mysalary.main;

import android.app.Application;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;

import com.henderson.mysalary.BaseViewModel;
import com.henderson.mysalary.pay.DailyPayActivity;
import com.henderson.mysalary.pay.ExpectedPayActivity;
import com.henderson.mysalary.pay.HourlyPayActivity;

public class MainViewModel extends BaseViewModel {

	public MainViewModel(@NonNull Application application) {
		super(application);
	}

	public void clickPerTime(View view) {
		Intent intent = new Intent(getApplication(), HourlyPayActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(intent);
	}

	public void clickPerDay(View view) {
		Intent intent = new Intent(getApplication(), DailyPayActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(intent);
	}

	public void clickExpected(View view){
		Intent intent = new Intent(getApplication(), ExpectedPayActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(intent);
	}
}
