package com.henderson.mysalary;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.henderson.mysalary.model.Job;
import com.henderson.mysalary.utils.Constant;
import com.henderson.mysalary.utils.db.AppDatabase;

import java.util.List;

public class BaseViewModel extends AndroidViewModel {

	protected Activity activity;

	public BaseViewModel(@NonNull Application application) {
		super(application);
	}

	protected void onCreate(Activity activity){
		this.activity = activity;
		log("onResume", "");
	}

	protected void onResume(Activity activity){
		this.activity = activity;
		log("onResume", "");
	}

	public boolean checkJob(){
		List<Job> jobList = AppDatabase.getAppDatabase(activity).getJobList();
		if(jobList.size() == 0) {
			return true;
		}
		else {
			return false;
		}
	}

	//Example : log("getLocalIpAddress", "ip : " + ip);
	protected void log(String methodName, String msg){
		if(BuildConfig.DEBUG) Log.d("!!" + getClass().getSimpleName(), methodName + " " + msg);
	}
}
