package com.henderson.mysalary.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "tb_job")
public class Job {

	@PrimaryKey
	@ColumnInfo(name = "name")
	@NonNull
	public String jobName;

	@ColumnInfo(name = "color")
	public String jobColor;

	@ColumnInfo(name = "payday")
	public int payday;

	@ColumnInfo(name = "payment")
	public int payment;

	@Ignore
	private List<DailyPay> dailyPayList = new ArrayList<>();

	public List<DailyPay> getDailyPayList() {
		return dailyPayList;
	}

	public void setDailyPayList(List<DailyPay> dailyPayList) {
		this.dailyPayList = dailyPayList;
	}
}
