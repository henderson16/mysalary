package com.henderson.mysalary.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "tb_dailypay", foreignKeys = @ForeignKey(entity = Job.class, parentColumns = "name", childColumns = "name", onDelete = CASCADE))
public class DailyPay {
	@PrimaryKey(autoGenerate = true)
	@ColumnInfo(name = "id")
	@NonNull
	public int id;

	//0 means hourlypay, 1 means dailypay
	@ColumnInfo(name = "salary_type")
	public int salaryType;

	@ColumnInfo(name = "name")
	public String jobName;

	@ColumnInfo(name = "year")
	public int year;

	@ColumnInfo(name = "month")
	public int month;

	@ColumnInfo(name = "date")
	public int date;

	@ColumnInfo(name = "working_time")
	public int workingTime;

	@ColumnInfo(name = "hourly_payment")
	public int hourlyPayment;

	@ColumnInfo(name = "daily_payment")
	public int dailyPayment;
}
